import { defineConfig } from "vite";
import react from "@vitejs/plugin-react-swc";

// https://vitejs.dev/config/
export default defineConfig({
	// base: "E:/Workspace/Work/playground_23101281/react-pywebview/frontend",
	base: "",
	plugins: [react()],
});
